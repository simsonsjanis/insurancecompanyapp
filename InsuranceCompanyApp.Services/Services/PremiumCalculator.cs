﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.Interfaces;

namespace InsuranceCompanyApp.Services.Services
{
    public class PremiumCalculator : IPremiumCalculator
    {
        private IPolicyRepo policyRepo;
        private IPolicyRiskRepo policyRiskRepo;
        private IRiskRepo riskRepo;
        private IList<PolicyRisk> policyRisks = new List<PolicyRisk>();

        public PremiumCalculator(IPolicyRepo policyRepo, IPolicyRiskRepo policyRiskRepo, IRiskRepo riskRepo)
        {
            this.policyRepo = policyRepo;
            this.policyRiskRepo = policyRiskRepo;
            this.riskRepo = riskRepo;
        }


        public void UpdatePolicyPremium(int policyId)
        {
            var policy = policyRepo.Get(policyId);
            policy.Premium = 0m;
            foreach (var policyRisk in policy.InsuredRisks)
            {
                if (policyRisk.PricePerPeriod == 0.00m)
                {
                    policyRisk.PricePerPeriod = CalculatePricePerPeriod(policyRisk, policy);
                }
                policy.Premium += policyRisk.PricePerPeriod;
                policyRisks.Add(policyRisk);
            }
            policyRiskRepo.Update(policyRisks);
            policyRepo.Update(policy);
            policyRisks.Clear();
        }

        public void CalculatePolicyPremiums(IList<Policy> policies)
        {
            foreach (var policy in policies)
            {
                policy.Premium = 0m;
                foreach (var risk in policy.InsuredRisks)
                {
                    policy.Premium += risk.PricePerPeriod;
                }
            }
        }

        private decimal CalculatePricePerPeriod(PolicyRisk policyRisk, IPolicy policy)
        {
            var monthsLeft = ((policy.ValidTill.Year - policyRisk.ValidFrom.Year) * 12) + policy.ValidTill.Month -
                             policyRisk.ValidFrom.Month;
            var risk = riskRepo.Get(policyRisk.RiskId);
            var pricePerMonth = risk.YearlyPrice / 12;

            return monthsLeft * pricePerMonth;
        }
    }
}
