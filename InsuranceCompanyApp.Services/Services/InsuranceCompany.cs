﻿using System;
using System.Collections.Generic;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Services
{
    public class InsuranceCompany : IInsuranceCompany
    {
        private readonly IRiskService riskService;
        private readonly IPolicyService policyService;
        private readonly IPolicyRiskService policyRiskService;
        private string name = "NotAScam inc.";

        public InsuranceCompany(IRiskService riskService, IPolicyService policyService, IPolicyRiskService policyRiskService)
        {
            this.riskService = riskService;
            this.policyService = policyService;
            this.policyRiskService = policyRiskService;
        }

        /// <inheritdoc />
        public string Name => name;

        public void ChangeCompanyName(string newName)
        {
            name = newName;
        }

        /// <inheritdoc />
        public IList<Risk> AvailableRisks => riskService.GetAvailableRisks();

        /// <inheritdoc />
        public void AddNewAvailableRisk(Risk risk)
        {
            var riskViewModel = riskService.MapToRiskViewModel(risk);
            riskService.AddNewRiskToDb(riskViewModel);
        }

        /// <inheritdoc />
        public void ModifyAvailableRisk(Risk risk)
        {
            var riskViewModel = riskService.MapToRiskViewModel(risk);
            riskService.UpdateRisk(riskViewModel);
        }

        /// <inheritdoc />
        public bool DeleteAvailableRisk(Risk risk)
        {
            return riskService.DeleteRisk(risk.Id);
        }

        /// <inheritdoc />
        public IPolicy SellPolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks)
        {
            var policy = new Policy(nameOfInsuredObject, validFrom, validFrom.AddMonths(validMonths), selectedRisks);
            var policyViewModel = policyService.MapToPolicyViewModel(policy);
            if (policyService.IsObjectAlreadyInsured(policyViewModel))
            {
                throw new ArgumentException("Object is already insured for the given time period!");
            }

            if (policyService.ValidatePolicyDates(policyViewModel) != "success")
            {
                throw new ArgumentException("Invalid time period!");
            }

            return policyService.AddNewPolicyToDb(policyViewModel);
        }

        /// <inheritdoc />
        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom)
        {
            var policy = GetPolicy(nameOfInsuredObject, validFrom);
            var policyRiskViewModel = new PolicyRiskViewModel()
            {
                AvailableRisks = new List<Risk>() { risk },
                PolicyId = policy.Id,
                RiskName = risk.Name,
                ValidFrom = validFrom
            };

            if (policyService.ValidatePolicyRiskViewModel(policyRiskViewModel) != "success")
            {
                throw new ArgumentException("Valid From must be between the start and end date of the policy!");
            }

            if (policyRiskService.DoesRiskAlreadyExist(policyRiskViewModel))
            {
                throw new ArgumentException("Policy already has this type of risk insured!");
            }

            policyRiskService.AddNewInsuredRisk(policyRiskViewModel);
        }

        /// <inheritdoc />
        public IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            IPolicy policy = policyService.GetPolicyByEffectiveDate(nameOfInsuredObject, effectiveDate);
            var policyRiskViewModelCollection = policyRiskService.GetPolicyRiskViewModelsByEffectiveDate(policy.Id, effectiveDate);
            foreach (var policyRiskViewModel in policyRiskViewModelCollection.PolicyRiskViewModels)
            {
                var policyRisk = policyRiskService.MapToPolicyRiskModel(policyRiskViewModel);
                policy.InsuredRisks.Add(policyRisk);

            }

            return policy;
        }
    }
}
