﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Services
{
    public class PolicyService : IPolicyService
    {
        private readonly IPolicyRepo policyRepo;
        private readonly IPremiumCalculator premiumCalculator;

        public PolicyService(IRiskRepo riskRepo, IPolicyRepo policyRepo, IPolicyRiskRepo policyRiskRepo, IPremiumCalculator premiumCalculator)
        {
            this.policyRepo = policyRepo;
            this.premiumCalculator = premiumCalculator;
        }

        public PolicyViewModelCollection GetPolicyViewModelsByEffectiveDate(DateTime effective)
        {
            IList<Policy> policies = policyRepo.GetByEffectiveDate(effective);
            premiumCalculator.CalculatePolicyPremiums(policies);
            var policyViewModels = new List<PolicyViewModel>();
            foreach (Policy policy in policies)
            {
                policyViewModels.Add(MapToPolicyViewModel(policy));
            }

            var policyViewModelCollection = new PolicyViewModelCollection()
            {
                EffectiveDate = effective,
                PolicyViewModels = policyViewModels
            };

            return policyViewModelCollection;
        }

        public bool DeletePolicy(int policyId)
        {
            return policyRepo.Remove(policyId);
        }

        public string ValidatePolicyRiskViewModel(PolicyRiskViewModel model)
        {
            var policy = policyRepo.Get(model.PolicyId);
            if (model.ValidFrom < DateTime.Today ||
                model.ValidFrom > policy.ValidTill ||
                model.ValidFrom < policy.ValidFrom ||
                (policy.ValidTill - model.ValidFrom < TimeSpan.FromDays(30)))
            {
                return
                    "Valid From must be between the start and end date of the policy! Also it cannot be in the past and" +
                    "there must be at least one month in between the valid from date and the policy end date.";
            }

            return "success";
        }

        public bool IsObjectAlreadyInsured(PolicyViewModel model)
        {
            var existingPolicies = policyRepo.Get(model.NameOfInsuredObject, model.ValidFrom);
            if (existingPolicies == null || existingPolicies.Count == 0)
            {
                return false;
            }

            return existingPolicies.Any(existingPolicy => model.ValidFrom < existingPolicy.ValidTill);
        }

        public IPolicy AddNewPolicyToDb(PolicyViewModel model)
        {
            IPolicy policy = new Policy(model.NameOfInsuredObject, model.ValidFrom, model.ValidTill);
            policyRepo.Add(policy);
            return policy;
        }

        public PolicyViewModel MapToPolicyViewModel(Policy policy)
        {
            var pvm = new PolicyViewModel()
            {
                Id = policy.Id,
                NameOfInsuredObject = policy.NameOfInsuredObject,
                Premium = policy.Premium,
                ValidFrom = policy.ValidFrom,
                ValidTill = policy.ValidTill,
            };
            return pvm;
        }

        /// <inheritdoc />
        public IPolicy GetPolicyByEffectiveDate(string nameOfInsuredObject, DateTime effectiveDate)
        {
            var models = GetPolicyViewModelsByEffectiveDate(effectiveDate);
            PolicyViewModel policyViewModel = models.PolicyViewModels.FirstOrDefault(x => x.NameOfInsuredObject == nameOfInsuredObject);

            IPolicy policy = MapToPolicy(policyViewModel);

            return policy;
        }

        private IPolicy MapToPolicy(PolicyViewModel policyViewModel)
        {
            return new Policy(policyViewModel.NameOfInsuredObject, policyViewModel.ValidFrom, policyViewModel.ValidTill);
        }

        public string ValidatePolicyDates(PolicyViewModel model)
        {
            if (model.ValidFrom < DateTime.Today || model.ValidFrom >= model.ValidTill)
            {
                return "Valid From cannot be in the past and must be before Valid Till date";
            }

            return model.ValidTill < DateTime.Today.AddMonths(1)
                ? "Valid Till must be at least one month from today!"
                : "success";
        }

        /// <inheritdoc />
        public PolicyViewModel GetPolicyViewModel(int policyId)
        {
            var policy = policyRepo.Get(policyId);
            return policy == null ? null : MapToPolicyViewModel(policy);
        }
    }
}
