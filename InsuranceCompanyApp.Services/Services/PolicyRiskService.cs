﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Services
{
    public class PolicyRiskService : IPolicyRiskService
    {
        private readonly IRiskRepo riskRepo;
        private readonly IPolicyRiskRepo policyRiskRepo;
        private readonly IPremiumCalculator premiumCalculator;

        public PolicyRiskService(IRiskRepo riskRepo, IPolicyRepo policyRepo, IPolicyRiskRepo policyRiskRepo,
            IPremiumCalculator premiumCalculator)
        {
            this.riskRepo = riskRepo;
            this.policyRiskRepo = policyRiskRepo;
            this.premiumCalculator = premiumCalculator;
        }

        public PolicyRiskViewModelCollection GetPolicyRiskViewModelsByEffectiveDate(int? id, DateTime effective)
        {
            IList<PolicyRisk> policyRisks = policyRiskRepo.GetPolicyRisks((int)id)
                .Where(x => x.ValidFrom <= effective).ToList();

            IList<PolicyRiskViewModel> prViewModels = new List<PolicyRiskViewModel>();

            foreach (var policyRisk in policyRisks)
            {
                prViewModels.Add(MapToPolicyRiskViewModel(policyRisk));
            }

            var policyRiskViewModelCollection = new PolicyRiskViewModelCollection()
            {
                EffectiveDate = effective,
                PolicyRiskViewModels = prViewModels,
                PolicyId = id.Value
            };

            return policyRiskViewModelCollection;
        }

        public bool DeletePolicyRisk(int policyRiskId, int policyId)
        {
            if (!policyRiskRepo.Remove(policyRiskId))
            {
                return false;
            }

            premiumCalculator.UpdatePolicyPremium(policyId);
            return true;
        }

        public bool DeletePolicyRisks(int policyId)
        {
            var policyRisks = policyRiskRepo.GetPolicyRisks(policyId);
            return policyRiskRepo.Remove(policyRisks);
        }

        public bool DoesRiskAlreadyExist(PolicyRiskViewModel model)
        {
            return policyRiskRepo.IsDuplicate(model.PolicyId, model.RiskName);
        }

        public void AddNewInsuredRisk(PolicyRiskViewModel model)
        {
            Risk risk = riskRepo.Get(model.RiskName);
            PolicyRisk pr = new PolicyRisk(risk.Id, model.PolicyId, model.ValidFrom);
            policyRiskRepo.Add(pr);

            premiumCalculator.UpdatePolicyPremium(model.PolicyId);
        }

        private PolicyRiskViewModel MapToPolicyRiskViewModel(PolicyRisk policyRisk)
        {
            var model = new PolicyRiskViewModel()
            {
                Id = policyRisk.Id,
                PricePerPeriod = policyRisk.PricePerPeriod,
                RiskName = policyRisk.Risk.Name,
                ValidFrom = policyRisk.ValidFrom,
                PolicyId = policyRisk.PolicyId
            };
            return model;
        }

        public PolicyRisk MapToPolicyRiskModel(PolicyRiskViewModel policyRiskView)
        {
            var risk = riskRepo.Get(policyRiskView.RiskName);
            var model = new PolicyRisk()
            {
                Id = policyRiskView.Id,
                PricePerPeriod = policyRiskView.PricePerPeriod,
                PolicyId = policyRiskView.PolicyId,
                ValidFrom = policyRiskView.ValidFrom,
                RiskId = risk.Id
            };
            return model;
        }

        public PolicyRiskViewModel CreateEmptyPolicyRiskViewModel(int? id)
        {
            if (id != null)
            {
                return new PolicyRiskViewModel()
                {
                    PolicyId = (int)id,
                    ValidFrom = DateTime.Today
                };
            }

            return null;
        }
    }
}