﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Services
{
    public class RiskService : IRiskService
    {
        private readonly IRiskRepo riskRepo;
        private readonly IPremiumCalculator premiumCalculator;

        public RiskService(IRiskRepo riskRepo, IPolicyRepo policyRepo, IPolicyRiskRepo policyRiskRepo,
            IPremiumCalculator premiumCalculator)
        {
            this.riskRepo = riskRepo;
            this.premiumCalculator = premiumCalculator;
        }

        public IList<RiskViewModel> GetAllRiskViewModels()
        {
            IList<Risk> risks = riskRepo.GetAll();
            IList<RiskViewModel> riskViewModels = new List<RiskViewModel>();
            foreach (var risk in risks)
            {
                riskViewModels.Add(MapToRiskViewModel(risk));
            }

            return riskViewModels;
        }

        public void AddNewRiskToDb(RiskViewModel model)
        {
            Risk risk = new Risk() { Name = model.Name, YearlyPrice = model.YearlyPrice };
            riskRepo.Add(risk);
        }

        public RiskViewModel GetRiskViewModel(int riskId)
        {
            var risk = riskRepo.Get(riskId);
            return risk == null ? null : MapToRiskViewModel(risk);
        }

        public void UpdateRisk(RiskViewModel model)
        {
            riskRepo.Update(new Risk() { Id = model.Id, YearlyPrice = model.YearlyPrice, Name = model.Name });
        }

        public bool DeleteRisk(int riskId)
        {
            return riskRepo.Remove(riskId);
        }

        public RiskViewModel MapToRiskViewModel(Risk risk)
        {
            var riskvm = new RiskViewModel() { Id = risk.Id, YearlyPrice = risk.YearlyPrice, Name = risk.Name };
            return riskvm;
        }


        public bool IsRiskTableEmpty()
        {
            return riskRepo.GetAll().Count == 0;
        }


        public IList<Risk> GetAvailableRisks()
        {
            return riskRepo.GetAll();
        }
    }
}