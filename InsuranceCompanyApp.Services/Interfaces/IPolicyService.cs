﻿using System;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Interfaces
{
    public interface IPolicyService
    {
        PolicyViewModelCollection GetPolicyViewModelsByEffectiveDate(DateTime effective);
        bool DeletePolicy(int policyId);
        string ValidatePolicyRiskViewModel(PolicyRiskViewModel model);
        bool IsObjectAlreadyInsured(PolicyViewModel model);
        IPolicy AddNewPolicyToDb(PolicyViewModel model);
        string ValidatePolicyDates(PolicyViewModel model);
        PolicyViewModel GetPolicyViewModel(int policyId);
        PolicyViewModel MapToPolicyViewModel(Policy policy);
        IPolicy GetPolicyByEffectiveDate(string nameOfInsuredObject, DateTime effectiveDate);
    }
}