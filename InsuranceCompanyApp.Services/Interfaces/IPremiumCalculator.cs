﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Services.Interfaces
{
    public interface IPremiumCalculator
    {
        void UpdatePolicyPremium(int policyId);
        void CalculatePolicyPremiums(IList<Policy> policies);
    }
}