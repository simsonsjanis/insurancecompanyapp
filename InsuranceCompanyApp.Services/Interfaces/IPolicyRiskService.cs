﻿using System;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Interfaces
{
    public interface IPolicyRiskService
    {
        PolicyRiskViewModelCollection GetPolicyRiskViewModelsByEffectiveDate(int? id, DateTime effective);
        bool DeletePolicyRisk(int policyRiskId, int policyId);
        bool DeletePolicyRisks(int policyId);
        bool DoesRiskAlreadyExist(PolicyRiskViewModel model);
        void AddNewInsuredRisk(PolicyRiskViewModel model);
        PolicyRiskViewModel CreateEmptyPolicyRiskViewModel(int? id);
        PolicyRisk MapToPolicyRiskModel(PolicyRiskViewModel policyRiskView);
    }
}