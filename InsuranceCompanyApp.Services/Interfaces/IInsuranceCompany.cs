﻿using System;
using System.Collections.Generic;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Services.Interfaces
{
    public interface IInsuranceCompany
    {
        /// <summary>
        /// Name of Insurance company
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Change the name of company
        /// </summary>
        /// <param name="newName"></param>
        void ChangeCompanyName(string newName);

        /// <summary>
        /// List of the risks that can be insured. List can be updated at any time
        /// </summary>
        IList<Risk> AvailableRisks { get; }

        /// <summary>
        /// Add new risk to AvailableRisks list
        /// </summary>
        /// <param name="risk"></param>
        void AddNewAvailableRisk(Risk risk);

        /// <summary>
        /// Modify an existing available risk
        /// </summary>
        /// <param name="risk"></param>
        void ModifyAvailableRisk(Risk risk);

        /// <summary>
        /// Delete an existing available risk. Returns 1 for success, 0 for failure.
        /// </summary>
        /// <param name="risk"></param>
        bool DeleteAvailableRisk(Risk risk);

        /// <summary>
        /// Sell the policy.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of the insured object. Must be unique in the given period.</param>
        /// <param name="validFrom">Date and time when policy starts. Can not be in the past</param>
        /// <param name="validMonths">Policy period in months</param>
        /// <param name="selectedRisks">List of risks that must be included in the policy</param>
        /// <returns>Information about policy</returns>
        IPolicy SellPolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks);
        /// <summary>
        /// Add risk to the policy of insured object.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of insured object</param>
        /// <param name="risk">Risk that must be added</param>
        /// <param name="validFrom">Date when risk becomes active. Can not be in the past</param>
        void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom);
        /// <summary>
        /// Gets policy with a risks at the given point of time.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of insured object</param>
        /// <param name="effectiveDate">Point of date and time, when the policy effective</param>
        /// <returns></returns>
        IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate);
    }
}
