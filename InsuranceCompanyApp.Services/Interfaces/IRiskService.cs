﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Models;
using InsuranceCompanyApp.Services.ViewModels;

namespace InsuranceCompanyApp.Services.Interfaces
{
    public interface IRiskService
    {
        IList<RiskViewModel> GetAllRiskViewModels();
        void AddNewRiskToDb(RiskViewModel model);
        RiskViewModel GetRiskViewModel(int riskId);
        void UpdateRisk(RiskViewModel model);
        bool DeleteRisk(int riskId);
        bool IsRiskTableEmpty();
        IList<Risk> GetAvailableRisks();
        RiskViewModel MapToRiskViewModel(Risk risk);
    }
}