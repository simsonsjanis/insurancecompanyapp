﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsuranceCompanyApp.Services.ViewModels
{
    public class PolicyRiskViewModelCollection
    {
        [Display(Name = "Policy Risks")]
        public IList<PolicyRiskViewModel> PolicyRiskViewModels { get; set; }

        [Display(Name = "Effective Date")]
        [DataType(DataType.Date)]
        public DateTime EffectiveDate { get; set; }
        public int PolicyId { get; set; }
    }
}
