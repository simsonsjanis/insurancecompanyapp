﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InsuranceCompanyApp.Services.ViewModels
{
    public class RiskViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [MinLength(1, ErrorMessage = "Risk name needs to be at least 1 character")]
        [DisplayName("Risk Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [DisplayName("Yearly Price")]
        public decimal YearlyPrice { get; set; }
    }
}
