﻿namespace InsuranceCompanyApp.Services.ViewModels
{
    public class ErrorViewModel
    {
        public string ErrorMessage { get; set; }
        public string RouteOfException { get; set; }
        public int StatusCode { get; set; }
        public string ErrorImagePath { get; set; }
    }
}
