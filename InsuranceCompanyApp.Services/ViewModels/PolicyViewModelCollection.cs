﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsuranceCompanyApp.Services.ViewModels
{
    public class PolicyViewModelCollection
    {
        public IList<PolicyViewModel> PolicyViewModels { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Effective Date")]
        public DateTime EffectiveDate { get; set; }
    }
}
