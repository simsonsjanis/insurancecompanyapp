﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceCompanyApp.Services.ViewModels
{
    public class PolicyViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Insured Object")]
        [Required(ErrorMessage = "Name is required")]
        [MinLength(1)]
        public string NameOfInsuredObject { get; set; }


        [Required]
        [Display(Name = "Valid From")]
        [DataType(DataType.Date)]
        public DateTime ValidFrom { get; set; }


        [Display(Name = "Valid Till")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime ValidTill { get; set; }

        [Display(Name = "Premium Total")]
        public decimal Premium { get; set; }

    }
}
