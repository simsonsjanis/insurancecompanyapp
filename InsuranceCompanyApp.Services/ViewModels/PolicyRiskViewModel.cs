﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Services.ViewModels
{
    public class PolicyRiskViewModel
    {
        public int Id { get; set; }
        public int PolicyId { get; set; }

        [Display(Name = "Risk Name")]
        public string RiskName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Valid From")]
        public DateTime ValidFrom { get; set; }

        [Display(Name = "Price Per Period")]
        public decimal PricePerPeriod { get; set; }

        [Display(Name = "Available Risks")]
        public IList<Risk> AvailableRisks { get; set; }
    }
}
