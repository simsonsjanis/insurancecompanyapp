﻿using System;
using System.Collections.Generic;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Data.Interfaces
{
    public interface IPolicyRepo
    {
        void Add(IPolicy policy);
        Policy Get(int id);
        void Truncate();
        IList<Policy> GetAll();
        void Update(Policy policy);
        IList<Policy> Get(string modelNameOfInsuredObject, DateTime validFrom);
        bool Remove(int policyId);
        int[] GetAllPolicyIds();
        IList<Policy> GetByEffectiveDate(DateTime effective);
    }
}