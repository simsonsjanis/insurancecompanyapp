﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Data.Interfaces
{
    public interface IRiskRepo
    {
        void Add(Risk risk);
        Risk Get(int riskId);
        void Truncate();
        bool IsDuplicate(Risk risk);
        IList<Risk> GetAll();
        void Update(Risk risk);
        bool Remove(int id);
        Risk Get(string riskName);
    }
}