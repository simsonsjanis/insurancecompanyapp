﻿using System.Collections.Generic;
using InsuranceCompanyApp.Data.Models;

namespace InsuranceCompanyApp.Data.Interfaces
{
    public interface IPolicyRiskRepo
    {
        void Add(PolicyRisk policyRisk);
        PolicyRisk Get(int Id);
        void Truncate();
        IList<PolicyRisk> GetPolicyRisks(int policyId);
        bool IsDuplicate(int policyId, string riskName);
        void Update(PolicyRisk policyRisk);
        bool Remove(int id);
        bool Remove(IList<PolicyRisk> policyRisks);
        void Update(IList<PolicyRisk> policyRisks);
    }
}