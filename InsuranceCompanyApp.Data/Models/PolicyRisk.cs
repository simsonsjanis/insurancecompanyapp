﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsuranceCompanyApp.Data.Models
{
    public class PolicyRisk
    {
        public PolicyRisk(int riskId, int policyId, DateTime validFrom)
        {
            this.RiskId = riskId;
            this.PolicyId = policyId;
            this.ValidFrom = validFrom;
        }
        public PolicyRisk() { }
        [Key]
        [Required]
        public int Id { get; set; }


        public int RiskId { get; set; }
        public Risk Risk { get; set; }


        public int PolicyId { get; set; }
        public Policy Policy { get; set; }

        [Required]
        public DateTime ValidFrom { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal PricePerPeriod { get; set; }
    }
}
