﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsuranceCompanyApp.Data.Models
{
    public class Risk
    {
        [Required]
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Unique name of the risk
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Risk yearly price
        /// </summary>
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal YearlyPrice { get; set; }

    }
}
