﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using InsuranceCompanyApp.Data.Interfaces;

namespace InsuranceCompanyApp.Data.Models
{
    public class Policy : IPolicy
    {
        public Policy(string nameOfInsuredObject, DateTime validFrom, DateTime validTill, IList<Risk> selectedRisks)
        {
            NameOfInsuredObject = nameOfInsuredObject;
            ValidFrom = validFrom;
            ValidTill = validTill;

            foreach (var selectedRisk in selectedRisks)
            {
                InsuredRisks.Add(new PolicyRisk(selectedRisk.Id, this.Id, DateTime.Today));
            }

        }

        public Policy() { }

        public Policy(string nameOfInsuredObject, DateTime validFrom, DateTime validTill)
        {
            NameOfInsuredObject = nameOfInsuredObject;
            ValidFrom = validFrom;
            ValidTill = validTill;
        }

        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        public string NameOfInsuredObject { get; set; }

        /// <inheritdoc />
        [Required]
        public DateTime ValidFrom { get; set; }

        /// <inheritdoc />
        [Required]
        public DateTime ValidTill { get; set; }

        /// <inheritdoc />
        [Column(TypeName = "decimal(18,2)")]
        public decimal Premium { get; set; }

        /// <inheritdoc />
        public List<PolicyRisk> InsuredRisks { get; set; } = new List<PolicyRisk>();
    }
}
