﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InsuranceCompanyApp.Data.Repositories
{
    public class PolicyRepo : IPolicyRepo
    {
        private readonly DbContextOptions options;

        public PolicyRepo(DbContextOptions options)
        {
            this.options = options;
        }

        public void Add(IPolicy policy)
        {
            using (var context = new InsuranceContext(options))
            {
                context.Policies.Add((Policy)policy);
                context.SaveChanges();
            }
        }

        public Policy Get(int id)
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Policies.Include(x => x.InsuredRisks).FirstOrDefault(c => c.Id == id);
            }
        }

        public IList<Policy> GetAll()
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Policies.Include(x => x.InsuredRisks).ToList();
            }
        }

        public void Update(Policy policy)
        {
            using (var context = new InsuranceContext(options))
            {
                context.Policies.Update(policy);
                context.SaveChanges();
            }
        }

        public IList<Policy> Get(string nameOfInsuredObject, DateTime validFrom)
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Policies.Where(c => c.NameOfInsuredObject == nameOfInsuredObject &&
                                                   validFrom < c.ValidTill).ToList();
            }
        }

        public bool Remove(int policyId)
        {
            using (var context = new InsuranceContext(options))
            {
                try
                {
                    var policyRisk = Get(policyId);
                    if (policyRisk == null)
                    {
                        return false;
                    }

                    context.Policies.Remove(policyRisk);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            ;
        }

        public int[] GetAllPolicyIds()
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Policies.Select(x => x.Id).ToArray();
            }
        }

        public IList<Policy> GetByEffectiveDate(DateTime effective)
        {
            using (var context = new InsuranceContext(options))
            {
                var policies = context.Policies.Where(x => x.ValidFrom <= effective)
                    .Include(c => c.InsuredRisks).ToList();
                foreach (var policy in policies)
                {
                    policy.InsuredRisks.RemoveAll(x => x.ValidFrom > effective);
                }

                return policies;
            }
        }

        public void Truncate()
        {
            using (var context = new InsuranceContext(options))
            {
                context.Policies.RemoveRange(context.Policies);
                context.SaveChanges();
            }
        }
    }
}