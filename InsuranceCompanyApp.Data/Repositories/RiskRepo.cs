﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InsuranceCompanyApp.Data.Repositories
{
    public class RiskRepo : IRiskRepo
    {
        private readonly DbContextOptions options;

        public RiskRepo(DbContextOptions options)
        {
            this.options = options;
        }

        public void Add(Risk risk)
        {
            using (var context = new InsuranceContext(options))
            {
                if (Get(risk.Name) == null)
                {
                    context.Risks.Add(risk);
                    context.SaveChanges();
                }
            }
        }

        public Risk Get(int riskId)
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Risks.FirstOrDefault(x => x.Id == riskId);
            }
        }

        public Risk Get(string riskName)
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Risks.FirstOrDefault(x => x.Name == riskName);
            };
        }

        /// <inheritdoc />
        public IList<Risk> GetAll()
        {
            using (var context = new InsuranceContext(options))
            {
                return context.Risks.ToList();
            };
        }

        /// <inheritdoc />
        public bool IsDuplicate(Risk risk)
        {
            var riskInDb = Get(risk.Name);
            return riskInDb != null;
        }

        public void Truncate()
        {
            using (var context = new InsuranceContext(options))
            {
                context.Risks.RemoveRange(context.Risks);
                context.SaveChanges();
            };
        }

        /// <inheritdoc />
        public void Update(Risk risk)
        {
            using (var context = new InsuranceContext(options))
            {
                var riskInDb = context.Risks.Find(risk.Id);
                riskInDb.YearlyPrice = risk.YearlyPrice;
                riskInDb.Name = risk.Name;
                context.SaveChanges();
            };
        }

        /// <inheritdoc />
        public bool Remove(int id)
        {
            using (var context = new InsuranceContext(options))
            {
                try
                {
                    Risk riskInDb = Get(id);
                    if (riskInDb == null)
                    {
                        return false;
                    }
                    context.Risks.Remove(riskInDb);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }
    }
}
