﻿using System.Linq;
using InsuranceCompanyApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InsuranceCompanyApp.Data.Repositories
{
    public class InsuranceContext : DbContext
    {
        public InsuranceContext() { }

        public InsuranceContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Policy> Policies { get; set; }
        public DbSet<Risk> Risks { get; set; }
        public DbSet<PolicyRisk> PolicyRisks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }
    }
}
