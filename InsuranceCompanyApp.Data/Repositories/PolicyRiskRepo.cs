﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InsuranceCompanyApp.Data.Repositories
{
    public class PolicyRiskRepo : IPolicyRiskRepo
    {
        private readonly DbContextOptions options;

        public PolicyRiskRepo(DbContextOptions options)
        {
            this.options = options;
        }

        public void Add(PolicyRisk policyRisk)
        {
            using (var context = new InsuranceContext(options))
            {
                context.PolicyRisks.Add(policyRisk);
                context.SaveChanges();
            }
        }

        public PolicyRisk Get(int Id)
        {
            using (var context = new InsuranceContext(options))
            {
                return context.PolicyRisks.Find(Id);
            }
        }

        public void Truncate()
        {
            using (var context = new InsuranceContext(options))
            {
                context.PolicyRisks.RemoveRange(context.PolicyRisks);
                context.SaveChanges();
            }
        }

        public IList<PolicyRisk> GetPolicyRisks(int policyId)
        {
            using (var context = new InsuranceContext(options))
            {
                var policyRisks = context.PolicyRisks.Include(x => x.Risk).Where(c => c.PolicyId == policyId);
                return policyRisks.ToList();
            }
        }

        public bool IsDuplicate(int policyId, string riskName)
        {
            var policyRisks = GetPolicyRisks(policyId);
            return policyRisks.FirstOrDefault(x => x.Risk.Name == riskName) != null;
        }

        public void Update(PolicyRisk policyRisk)
        {
            using (var context = new InsuranceContext(options))
            {
                context.PolicyRisks.Update(policyRisk);
                context.SaveChanges();
            }
        }

        /// <inheritdoc />
        public void Update(IList<PolicyRisk> policyRisks)
        {
            using (var context = new InsuranceContext(options))
            {
                context.PolicyRisks.UpdateRange(policyRisks);
                context.SaveChanges();
            }
        }

        public bool Remove(int id)
        {
            using (var context = new InsuranceContext(options))
            {
                try
                {
                    var policyRisk = Get(id);
                    if (policyRisk == null)
                    {
                        return false;
                    }
                    context.PolicyRisks.Remove(policyRisk);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public bool Remove(IList<PolicyRisk> policyRisks)
        {
            using (var context = new InsuranceContext(options))
            {
                try
                {
                    context.PolicyRisks.RemoveRange(policyRisks);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}