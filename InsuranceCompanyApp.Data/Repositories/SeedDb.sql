SET IDENTITY_INSERT [dbo].[Policies] ON 

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (2, N'Peter''s House', CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(N'2019-12-14T00:00:00.0000000' AS DateTime2), CAST(422.67 AS Decimal(18, 2)))

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (3, N'Starship Enterprise', CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-01-01T00:00:00.0000000' AS DateTime2), CAST(1698.00 AS Decimal(18, 2)))

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (4, N'Clone Troopers', CAST(N'2050-12-14T00:00:00.0000000' AS DateTime2), CAST(N'2100-12-14T00:00:00.0000000' AS DateTime2), CAST(2000.00 AS Decimal(18, 2)))

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (7, N'TARDIS', CAST(N'0017-05-12T00:00:00.0000000' AS DateTime2), CAST(N'5742-05-12T00:00:00.0000000' AS DateTime2), CAST(22896.00 AS Decimal(18, 2)))

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (8, N'Asgard Starship', CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(N'2023-12-14T00:00:00.0000000' AS DateTime2), CAST(1694.00 AS Decimal(18, 2)))

INSERT [dbo].[Policies] ([Id], [NameOfInsuredObject], [ValidFrom], [ValidTill], [Premium]) VALUES (9, N'Jurassic Park', CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(N'2033-12-14T00:00:00.0000000' AS DateTime2), CAST(7385.00 AS Decimal(18, 2)))

SET IDENTITY_INSERT [dbo].[Policies] OFF

SET IDENTITY_INSERT [dbo].[Risks] ON 

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (1, N'Fire', CAST(102.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (2, N'Theft', CAST(150.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (3, N'Flood', CAST(80.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (4, N'Alien Invasion', CAST(250.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (5, N'Tornado', CAST(320.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (6, N'Animal Attack', CAST(65.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (7, N'Explosion', CAST(512.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (8, N'Friendly Fire', CAST(40.00 AS Decimal(18, 2)))

INSERT [dbo].[Risks] ([Id], [Name], [YearlyPrice]) VALUES (9, N'Dimensional Locking', CAST(4.00 AS Decimal(18, 2)))

SET IDENTITY_INSERT [dbo].[Risks] OFF

SET IDENTITY_INSERT [dbo].[PolicyRisks] ON 

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (7, 1, 2, CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(102.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (8, 2, 2, CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(150.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (10, 7, 2, CAST(N'2019-08-23T00:00:00.0000000' AS DateTime2), CAST(170.67 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (11, 4, 3, CAST(N'2019-12-14T00:00:00.0000000' AS DateTime2), CAST(520.83 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (12, 7, 3, CAST(N'2019-12-14T00:00:00.0000000' AS DateTime2), CAST(1066.67 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (13, 1, 3, CAST(N'2020-12-14T00:00:00.0000000' AS DateTime2), CAST(110.50 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (14, 8, 4, CAST(N'2050-12-14T00:00:00.0000000' AS DateTime2), CAST(2000.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (15, 9, 7, CAST(N'0018-05-12T00:00:00.0000000' AS DateTime2), CAST(22896.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (16, 1, 8, CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(510.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (17, 4, 8, CAST(N'2019-04-18T00:00:00.0000000' AS DateTime2), CAST(1166.67 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (18, 9, 8, CAST(N'2019-08-15T00:00:00.0000000' AS DateTime2), CAST(17.33 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (19, 6, 9, CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(975.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (20, 2, 9, CAST(N'2018-12-14T00:00:00.0000000' AS DateTime2), CAST(2250.00 AS Decimal(18, 2)))

INSERT [dbo].[PolicyRisks] ([Id], [RiskId], [PolicyId], [ValidFrom], [PricePerPeriod]) VALUES (21, 5, 9, CAST(N'2020-12-14T00:00:00.0000000' AS DateTime2), CAST(4160.00 AS Decimal(18, 2)))

SET IDENTITY_INSERT [dbo].[PolicyRisks] OFF

