﻿using System.IO;
using System.Linq;
using System.Reflection;
using InsuranceCompanyApp.Data.Interfaces;
using InsuranceCompanyApp.Data.Repositories;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace InsuranceCompanyApp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public IConfiguration Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddDbContext<InsuranceContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("InsuranceDbConnection")));

            services.AddMvc();
            services.AddScoped<IPolicyRepo, PolicyRepo>();
            services.AddScoped<IPolicyRiskRepo, PolicyRiskRepo>();
            services.AddScoped<IRiskRepo, RiskRepo>();
            services.AddScoped<IPolicyService, PolicyService>();
            services.AddScoped<IRiskService, RiskService>();
            services.AddScoped<IPolicyRiskService, PolicyRiskService>();
            services.AddScoped<IPremiumCalculator, PremiumCalculator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // this stuff here is the middleware, basically what code needs to execute when a request comes in
            // order is important here
            UpdateDatabase(app);
            app.UseBrowserLink();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
            }

            app.UseStaticFiles();
            app.UseNodeModules(env);

            // enables listening to requests and trying to map them to controllers which then would map to views
            app.UseMvc(cfg =>
            {
                cfg.MapRoute("DefaultRoute",
                    "/{controller}/{action}/{id?}",
                    new { controller = "App", Action = "Index" });
            });

        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            var pathToFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                @"Repositories\SeedDb.sql");

            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<InsuranceContext>())
                {
                    context.Database.Migrate();
                    if (context.Policies.Count() == 0 &&
                        context.PolicyRisks.Count() == 0 &&
                        context.Risks.Count() == 0)
                    {
                        context.Database.ExecuteSqlCommand(File.ReadAllText(pathToFile));
                    }
                }
            }


        }
    }
}
