﻿using InsuranceCompanyApp.Services.ViewModels;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InsuranceCompanyApp.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HandleError(int statusCode)
        {
            var statusData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            var model = new ErrorViewModel();

            switch (statusCode)
            {
                case 404:
                    model.ErrorMessage = "Page could not be found!";
                    model.ErrorImagePath = "/img/NotFound.jpg";
                    break;
                default:
                    model.ErrorMessage = "Oops! Something went wrong with your request!";
                    model.ErrorImagePath = "/img/robot.jpg";
                    break;
            }

            model.RouteOfException = statusData.OriginalPath;
            model.StatusCode = statusCode;

            return View(model);
        }
    }
}
