﻿using System.Collections.Generic;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.Services;
using InsuranceCompanyApp.Services.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceCompanyApp.Controllers
{
    public class RiskController : Controller
    {
        private readonly IRiskService riskService;

        public RiskController(IRiskService riskService)
        {
            this.riskService = riskService;
        }

        [HttpGet]
        public IActionResult Risk()
        {
            IList<RiskViewModel> riskViewModels = riskService.GetAllRiskViewModels();

            return View(riskViewModels);
        }


        public IActionResult DetailsRisk(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var riskViewModel = riskService.GetRiskViewModel((int)id);

            if (riskViewModel == null)
            {
                return NotFound();
            }

            return View(riskViewModel);
        }


        public IActionResult CreateEditRisk(int? id)
        {
            if (id == null)
            {
                ViewData["Title"] = "Create";
                return View();
            }

            ViewData["Title"] = "Edit";

            var riskViewModel = riskService.GetRiskViewModel((int)id);


            if (riskViewModel == null)
            {
                return NotFound();
            }

            return View(riskViewModel).WithInfo("Note:", "Updating risk price will affect only future policies.");
        }

        [HttpPost]
        public IActionResult CreateEditRisk(RiskViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View().WithWarning("Warning!", "Invalid data submitted, request has been ignored.");
            }

            if (model.Id == 0)
            {
                riskService.AddNewRiskToDb(model);
                ModelState.Clear();
                return RedirectToAction(nameof(Risk)).WithSuccess("Success!", "New risk has been created!");

            }

            riskService.UpdateRisk(model);
            return RedirectToAction(nameof(Risk)).WithSuccess("Success!", "Risk has been modified!");
        }

        public IActionResult DeleteRisk(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var riskViewModel = riskService.GetRiskViewModel((int)id);

            if (riskViewModel == null)
            {
                return NotFound();
            }

            return View(riskViewModel)
                .WithInfo("Note:", "Risk can only be deleted when it is not used in any policy.");
        }

        [HttpPost, ActionName("DeleteRisk")]
        public IActionResult RemoveRisk(int id)
        {
            if (!riskService.DeleteRisk(id))
            {
                return RedirectToAction(nameof(Risk)).WithDanger("Failed to delete the risk!",
                    "Most likely a policy depends on this risk, so it is forbidden to remove it!");
            }

            return RedirectToAction(nameof(Risk)).WithSuccess("Success!", "Risk deleted!");
        }
    }
}