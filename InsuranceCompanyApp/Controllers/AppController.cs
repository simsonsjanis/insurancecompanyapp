﻿using Microsoft.AspNetCore.Mvc;

namespace InsuranceCompanyApp.Controllers
{
    public class AppController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
