﻿using System;
using InsuranceCompanyApp.Services.Interfaces;
using InsuranceCompanyApp.Services.Services;
using InsuranceCompanyApp.Services.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceCompanyApp.Controllers
{
    public class PolicyController : Controller
    {
        private readonly IRiskService riskService;
        private readonly IPolicyService policyService;
        private readonly IPolicyRiskService policyRiskService;
        public PolicyController(IPolicyService policyService, IPolicyRiskService policyRiskService, IRiskService riskService)
        {
            this.policyService = policyService;
            this.policyRiskService = policyRiskService;
            this.riskService = riskService;
        }

        [HttpPost]
        public IActionResult Policy(PolicyViewModelCollection model)
        {
            return RedirectToAction(nameof(Policy), new { validFrom = model.EffectiveDate });
        }

        [HttpGet]
        public IActionResult Policy(DateTime? validFrom)
        {
            DateTime effective = DateTime.MaxValue;
            if (validFrom.HasValue)
            {
                effective = validFrom.Value;
            }

            var policyViewModelCollection = policyService.GetPolicyViewModelsByEffectiveDate(effective);

            return View(policyViewModelCollection);
        }


        public IActionResult DetailsPolicy(int? id, DateTime? validFrom)
        {
            if (id == null)
            {
                return NotFound();
            }

            DateTime effective = DateTime.MaxValue;
            if (validFrom.HasValue)
            {
                effective = validFrom.Value;
            }

            var policyRiskViewModelCollection = policyRiskService.GetPolicyRiskViewModelsByEffectiveDate(id, effective);

            return policyRiskViewModelCollection.PolicyRiskViewModels.Count == 0
                ? View(policyRiskViewModelCollection).WithInfo("Note", "Policy does not have any insured risks at this time period!")
                : View(policyRiskViewModelCollection);
        }


        public IActionResult DeletePolicyRisk(int? id, int policyId)
        {
            if (id == null)
            {
                return NotFound();
            }

            return policyRiskService.DeletePolicyRisk((int)id, policyId)
                ? RedirectToAction(nameof(Policy)).WithSuccess("Success!", "Policy risk deleted!")
                : RedirectToAction(nameof(Policy)).WithDanger("Failed!", "Policy risk could not be deleted!");
        }


        public IActionResult CreatePolicyRisk(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (riskService.IsRiskTableEmpty())
            {
                return View().WithWarning("Warning!", "There are no risks added to the system! Please navigate to Risks section and add a new risk to the company!");
            }
            PolicyRiskViewModel policyRiskViewModel = policyRiskService.CreateEmptyPolicyRiskViewModel(id);
            policyRiskViewModel.AvailableRisks = riskService.GetAvailableRisks();

            return View(policyRiskViewModel);
        }

        [HttpPost]
        public IActionResult CreatePolicyRisk(PolicyRiskViewModel model)
        {
            model.AvailableRisks = riskService.GetAvailableRisks();
            if (!ModelState.IsValid)
            {
                return View(model).WithWarning("Warning!", "Invalid data submitted, request has been ignored.");
            }

            if (policyService.ValidatePolicyRiskViewModel(model) != "success")
            {
                return View(model).WithWarning("Warning!", "Valid From must be between the start and end date of the policy!");
            }

            if (policyRiskService.DoesRiskAlreadyExist(model))
            {
                return View(model).WithDanger("Action not allowed!", "Policy already has this type of risk insured!");
            }

            policyRiskService.AddNewInsuredRisk(model);
            ModelState.Clear();

            return RedirectToAction(nameof(CreatePolicyRisk), model.PolicyId)
                .WithSuccess("Success!", "Risk added to the policy!");
        }

        public IActionResult CreatePolicy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreatePolicy(PolicyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View().WithWarning("Warning!", "Invalid input!");
            }

            if (policyService.IsObjectAlreadyInsured(model))
            {
                return View().WithDanger("Attention!", "Object is already insured for the given time period.\n" +
                                                       "If you want to create a policy in the future, then Valid From must be after the end of the existing policy!");
            }

            var status = policyService.ValidatePolicyDates(model);
            if (status != "success")
            {
                return View().WithDanger("Attention!", status);
            }

            int policyId = policyService.AddNewPolicyToDb(model).Id;

            return RedirectToAction(nameof(CreatePolicyRisk), new { id = policyId }).WithSuccess("Validation succeeded!",
                "Now you may add risks that need to be insured!");
        }

        public IActionResult DeletePolicy(int policyId)
        {
            PolicyViewModel policyViewModel = policyService.GetPolicyViewModel(policyId);

            if (policyViewModel == null)
            {
                return NotFound();
            }

            return View(policyViewModel).WithInfo("Note:", "Policy with all the insured risks will be deleted!");
        }


        [HttpPost, ActionName("DeletePolicy")]
        public IActionResult RemovePolicy(PolicyViewModel model)
        {
            if (!policyRiskService.DeletePolicyRisks(model.Id))
            {
                return RedirectToAction(nameof(Policy))
                    .WithDanger("Failed!", "Risks associated with the policy could not be deleted!");
            }

            return !policyService.DeletePolicy(model.Id)
                ? RedirectToAction(nameof(Policy)).WithDanger("Failed!", "Policy could not be deleted!")
                : RedirectToAction(nameof(Policy)).WithSuccess("Success!", "Policy along with all insured risks has been deleted!");
        }


    }
}